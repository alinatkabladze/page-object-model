from pages.herokuapp_page import HerokuAppPage
from steps.common import CommonOps


class HerokuAppStep(CommonOps, HerokuAppPage):
    def click_to_link(self, link):
        self.driver.find_element(*link).click()
