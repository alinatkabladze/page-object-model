from pages.form_authentication_page import FormAuthenticationPage
from steps.common import CommonOps


class FormAuthentication(CommonOps, FormAuthenticationPage):
    def navigate_to_form_page(self):
        self.wait_for(self.FORM_AUTH_LOCATOR).click()

    def enter_login_username(self, username):
        self.wait_for(self.FORM_USERNAME).send_keys(username)

    def enter_login_password(self, password):
        self.find(self.FORM_PASSWORD).send_keys(password)

    def click_login_button(self):
        self.find(self.FORM_SUBMIT_BTN).click()

    def check_login_logout_status(self):
        return self.wait_for(self.FORM_ALERT)

    def click_logout_button(self):
        self.find(self.LOGOUT_BTN).click()
