from selenium.webdriver.common.by import By


class DynamicControlsPage:
    DYNAMIC_CONTROL = (By.LINK_TEXT, "Dynamic Controls")
    CHECKBOX_LOCATOR = (By.XPATH, "//input[@type='checkbox']")
    INPUT_TEXT_LOCATOR = (By.XPATH, "//input[@type='text']")
    STATE_CHANGE_LOCATOR = (By.ID, "message")
