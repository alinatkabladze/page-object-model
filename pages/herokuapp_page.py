from selenium.webdriver.common.by import By


class HerokuAppPage:
    DRAG_AND_DRO = (By.XPATH, "//li[.//a[@href='/drag_and_drop']]")

    BROKEN_IMAGES = (By.XPATH, "//li[.//a[@href='/broken_images']]")

    DIGEST_AUTHENTICATION_USER_AND = (By.XPATH, "//li[.//a[@href='/digest_auth']]")

    FORM_AUTHENTICATION = (By.XPATH, "//li[.//a[@href='/login']]")

    CHECKBOXES = (By.XPATH, "//li[.//a[@href='/checkboxes']]")

    SORTABLE_DATA_TABLES = (By.XPATH, "//li[.//a[@href='/tables']]")

    GEOLOCATION = (By.XPATH, "//li[.//a[@href='/geolocation']]")

    FILE_DOWNLOAD = (By.XPATH, "//li[.//a[@href='/download']]")

    STATUS_CODES = (By.XPATH, "//li[.//a[@href='/status_codes']]")

    TYPOS = (By.XPATH, "//li[.//a[@href='/typos']]")

    NOTIFICATION_MESSAGES = (By.XPATH, "//li[.//a[@href='/notification_message']]")

    DISAPPEARING_ELEMENTS = (By.XPATH, "//li[.//a[@href='/disappearing_elements']]")

    MULTIPLE_WINDOWS = (By.XPATH, "//li[.//a[@href='/windows']]")

    SECURE_FILE_DOWNLOAD = (By.XPATH, "//li[.//a[@href='/download_secure']]")

    NESTED_FRAMES = (By.XPATH, "//li[.//a[@href='/nested_frames']]")

    KEY_PRESSES = (By.XPATH, "//li[.//a[@href='/key_presses']]")

    SHIFTING_CONTENT = (By.XPATH, "//li[.//a[@href='/shifting_content']]")

    DYNAMIC_CONTENT = (By.XPATH, "//li[.//a[@href='/dynamic_content']]")

    FLOATING_MENU = (By.XPATH, "//li[.//a[@href='/floating_menu']]")

    JAVA_ALERTS = (By.XPATH, "//li[.//a[@href='/javascript_alerts']]")

    ADD_REMOVE_ELEMENTS = (By.XPATH, "//li[.//a[@href='/add_remove_elements/']]")

    TESTING = (By.XPATH, "//li[.//a[@href='/abtest']]")

    BASIC_AUTH_USER_AND = (By.XPATH, "//li[.//a[@href='/basic_auth']]")

    REDIRECT_LINK = (By.XPATH, "//li[.//a[@href='/redirector']]")

    FILE_UPLOAD = (By.XPATH, "//li[.//a[@href='/upload']]")

    CONTEXT_MENU = (By.XPATH, "//li[.//a[@href='/context_menu']]")

    EXIT_INTENT = (By.XPATH, "//li[.//a[@href='/exit_intent']]")

    DYNAMIC_CONTROLS = (By.XPATH, "//li[.//a[@href='/dynamic_controls']]")

    ENTRY = (By.XPATH, "//li[.//a[@href='/entry_ad']]")

    DYNAMIC_LOADING = (By.XPATH, "//li[.//a[@href='/dynamic_loading']]")

    HOVERS = (By.XPATH, "//li[.//a[@href='/hovers']]")

    INPUTS = (By.XPATH, "//li[.//a[@href='/inputs']]")

    CHALLENGING_DOM = (By.XPATH, "//li[.//a[@href='/challenging_dom']]")

    DROPDOWN = (By.XPATH, "//li[.//a[@href='/dropdown']]")

    FRAMES = (By.XPATH, "//li[.//a[@href='/frames']]")

    JAVA_ONLOAD_EVENT_ERROR = (By.XPATH, "//li[.//a[@href='/javascript_error']]")

    HORIZONTAL_SLIDER = (By.XPATH, "//li[.//a[@href='/horizontal_slider']]")

    QUERY_MENUS = (By.XPATH, "//li[.//a[@href='/jqueryui/menu']]")

    FORGOT_PASSWORD = (By.XPATH, "//li[.//a[@href='/forgot_password']]")

    SHADOW_DOM = (By.XPATH, "//li[.//a[@href='/shadowdom']]")

    WYSIWYG_EDITOR = (By.XPATH, "//li[.//a[@href='/tinymce']]")

    SLOW_RESOURCES = (By.XPATH, "//li[.//a[@href='/slow']]")

    LARGE_DEEP_DOM = (By.XPATH, "//li[.//a[@href='/large']]")

    INFINITE = (By.XPATH, "//li[.//a[@href='/infinite_scroll']]")
